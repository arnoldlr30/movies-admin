@extends('layouts.app')

@section('content')

<div class="container">
  @foreach($movie as $item)
  <div class="display">
    <h2>Movie Details Services</h2>
    <h3>{{$item->title}} - {{$item->description}} - {{$item->category}}</h3> 
  </div>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Service</th>
      <th scope="col">Status</th>
      <th scope="col">Date</th>
      <th scope="col">Days</th>
      <th scope="col">Movie</th>
      <th scope="col">User</th>
      <th scope="col">Total</th>
    </tr>
  </thead>
  <tbody>
    @foreach($item->services as $service)
      <tr>
      <td>{{ $service->id }}</td>
      <td>{{ $service->service }}</td>
      <td>{{ $service->status }}</td>
      <td>{{ $service->date }}</td>
      <td>{{ $service->time_service }}</td>
      <td>{{ $service->movie }}</td>
      <td>{{ $service->user }}</td>
      <td>${{ $service->total }}</td>
      </tr>
    @endforeach
 </tbody>
</table>
  @endforeach
</div>

@endsection