<div class="container">
	<h3>Movies</h3>
  <a href="{{ route('new-movie') }}" class="new">New movie <i class="far fa-plus-square"></i></a>
	<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Tittle</th>
      <th scope="col">Description</th>
      <th scope="col">Category</th>
      <th scope="col">Service</th>
      <th scope="col">Year</th>
      <th scope="col"><i class="fas fa-cog center"></i></th>
    </tr>
  </thead>
  <tbody>
    @foreach($movies as $movie)
    <tr>
      <td>{{ $movie->id }}</td>
      <td>{{ $movie->title }}</td>
      <td>{{ $movie->description }}</td>
      <td>{{ $movie->category }}</td>
      <td>{{ $movie->service }}</td>
      <td>{{ $movie->year }}</td>
      <td><a href="{{ route('read', $movie->id) }}"><i class="fas fa-pencil icon"></i></a><a href="{{ route('show', $movie->id) }}"><i class="fas fa-bars icon"></i></a><a href=""><i class="fas fa-trash icon"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>
{{ $movies->links() }}
</div>
