@extends('layouts.app')

@section('content')

<form  method="POST" action="{{ route('create')}}">
  {{ csrf_field() }}
  <div class="container">
    <h3>New movie</h3>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label for="title">Title</label>
      <input type="text" name="title" value="{{$movie->title}}" class="form-control" placeholder="Movie Title">
    </div>
    <div class="form-group col-md-4">
      <label for="description">Description</label>
      <input name="description" type="text" value="{{$movie->description}}" class="form-control" placeholder="Description">
    </div>
  </div>
    <div class="form-group col-md-4">
      <label for="category">Category</label>
      <select name="category" class="form-control">
        <option selected >{{$movie->category}}</option>
        <option value="Action">Action</option>
        <option value="Comedy">Comedy</option>
        <option value="Drama">Drama</option>
        <option value="Fantasy">Fantasy</option>
        <option value="Horror">Horror</option>
        <option value="Mistery">Mistery</option>
        <option value="Romance">Romance</option>
      </select>
    </div>
    <div class="form-group col-md-4">
      <label for="year">Year</label>
      <input name="year" type="number" value="{{$movie->year}}" class="form-control" placeholder="2010">
    </div>
    <div class="form-group col-md-4">
      <label for="service">Service</label>
      <select name="service" value="" class="form-control">
        <option selected>{{$movie->service}}</option>
        <option value="For sale">For sale</option>
        <option value="Rent">Rent</option>
      </select>
    </div>
    <div class="form-group col-md-4">
      <label for="stock">Stock</label>
      <input name="stock" type="number" value="{{$movie->stock}}" class="form-control" placeholder="10">
    </div>
    <div class="form-group col-md-4">
      <label for="available">Available</label>
      <select name="available" value="" class="form-control">
        <option selected>{{$movie->available}}</option>
        <option value="Available">Available</option>
        <option value="Not available">Not available</option>
      </select>
    </div>
  </div>
  <div class="container form-group">
  <button type="submit" class="btn btn-primary">Edit movie</button>
  </div>
  @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
</form>

@endsection