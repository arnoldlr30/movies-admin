@extends('layouts.app')

@section('content')

<div class="container">
	<h2>Movies</h2>
	<div class="movies">
	@foreach($movies as $movie)
	<form method="POST">
			{{ csrf_field() }}
		<div class="card item" style="width: 24rem;">
		  <i class="fas fa-film"></i>
		  <div class="card-body">
		    <h4 class="card-title">{{$movie->title}}</h4>
		    <p class="card-text">{{$movie->description}}</p>
		    <span class="badge badge-warning">{{$movie->category}}</span>
		    <span class="badge badge-info">{{$movie->year}}</span><br>
		    @if($movie->service != 'For sale')
		    <a href="{{ route('get-movie', $movie->id, 'Sold')}}" class="btn btn-primary">Buy</a>
		    @else
		    <a href="{{ route('get-movie', $movie->id, 'Rent')}}" class="btn btn-success">Rent</a>
		    @endif
		  </div>
		</div>
		</form>
	@endforeach
	
</div>
{{ $movies->links() }}
</div>

@endsection