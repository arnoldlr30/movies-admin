<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{
    use SoftDeletes;
    protected $table = 'movies';
    protected $fillable = [
        'title',
        'category',
        'description',
        'year',
        'available',
        'service',
        'stock'  
    ];

    public function services(){
        return $this->hasMany('App\Models\Service', 'movie_id');
    }
}
