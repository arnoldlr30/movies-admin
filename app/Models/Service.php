<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    use SoftDeletes;
    protected $table = 'services';
    protected $fillable = [
        'movie_id',
        'user_id',
        'service',
        'status',
        'date',
        'time_service'
    ];

    protected $appends = ['movie', 'user', 'total', 'more'];

    public function getMovieAttribute()
    {
        return $this->movie()->pluck('title')->first();
    }

    public function getUserAttribute(){
        return $this->user()->pluck('name')->first();
    }
    public function getTotalAttribute(){
        $total = 0;
        if ($this->status != 'Sold')
            $total = ($this->time_service * 0.50);
        else
            $total = 5.00;
        
        return $total;
    }

    public function getMoreAttribute(){

    }
    public function movie(){
    	return $this->belongsTo('App\Models\Movie', 'movie_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

}
