<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $movies = Movie::orderBy('id', 'desc')->paginate(10);

        return view('home', compact('movies'));

    }

    public function movies()
    {
        $movies = Movie::orderBy('id', 'desc')->paginate(10);

        return view('clients.movies', compact('movies'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->validate([
            'title'         => 'required',
            'description'   => 'required',
            'category'      => 'required',
            'year'          => 'required',
            'service'       => 'required',
            'stock'         => 'required',
            'available'     => 'required'
        ]);
        
        if($request->id)
            $movie = Movie::findOrFail($request->id);
        else
            $movie = new Movie;
        
        $movie->fill($request->all());
        $movie->save();

        return redirect('home')->with('status', 'Movie Saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function read($id)
    {
        $movie = Movie::findOrFail($id);

        return view('movies.edit', compact('movie'));
    }

    public function show($id)
    {
        $movie = Movie::where('id', $id)->with('services')->get();

        return view("movies.movie", compact('movie'));
        //return Response()->json([$movie]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
       
    }

    public function destroy(Movie $movie)
    {
        //
    }
}
