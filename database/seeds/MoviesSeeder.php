<?php

use Illuminate\Database\Seeder;
use App\Models\Movie;

class MoviesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 1; $i <= 20 ; $i++)
        {
            $table = new Movie;
            $table->title       = $faker->sentence(3);
            $table->description  = $faker->sentence(5);
            $table->category  = $faker->randomElement(['Action', 'Comedy', 'Drama', 'Fantasy', 'Horror', 'Mystery', 'Romance']);
            $table->year = $faker->numberBetween(1990, 2020);
            $table->available = $faker->randomElement(['available', 'Not available']);
            $table->service       = $faker->randomElement(['For sale', 'Rent']);
            $table->stock = $faker->numberBetween(1,15);
            
            $table->save();

        }
    }
}
