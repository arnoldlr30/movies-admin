<?php

use Illuminate\Database\Seeder;
use App\Models\Service;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $faker = Faker\Factory::create();

        for($i = 1; $i <= 60 ; $i++)
        {
            $table = new Service;
            $table->service       = $faker->randomElement(['For sale', 'Rent']);
            $table->status       = $faker->randomElement(['On loan', 'Returned', 'Sold']);
            $table->date = $faker->dateTimeBetween('2020-02-22 14:30:35', '2021-02-22 14:30:35');
            $table->time_service = $faker->numberBetween(1,14);
            $table->movie_id = $faker->numberBetween(1,20);
            $table->user_id = 1;
            
            $table->save();

        }
    }
}
