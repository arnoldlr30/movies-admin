<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
      User::create(['name' => 'Administrator', 'email' => 'admin@admin.com', 'password' => Hash::make('Admin123'), 'type' => 'Administrator']);
    }
}
